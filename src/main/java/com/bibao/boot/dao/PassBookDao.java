package com.bibao.boot.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bibao.boot.entity.PassBookEntity;

public interface PassBookDao extends JpaRepository<PassBookEntity, Integer> {

}
