package com.bibao.boot.dao;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.entity.PassBookEntity;
import com.bibao.boot.jpaallocationsize.JpaAllocationsizeApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = JpaAllocationsizeApplication.class)
public class PassBookDaoIntegTest {
	private static final Random RANDOM = new Random();
	
	@Autowired
	private PassBookDao dao;
	
	@Test
	public void testSaveAll() {
		List<PassBookEntity> entities = new ArrayList<>();
		for (int i=0; i<5; i++) entities.add(createEntity());
		dao.saveAll(entities);
	}

	private PassBookEntity createEntity() {
		PassBookEntity entity = new PassBookEntity();
		String plainText = randomString();
		entity.setPlainText(plainText);
		entity.setCipherText(Base64.getEncoder().encodeToString(plainText.getBytes()));
		return entity;
	}
	
	private String randomString() {
		StringBuffer sb = new StringBuffer();
		int n = 10 + RANDOM.nextInt(10);
		for (int i=0; i<n; i++) {
			char c = (char)('A' + RANDOM.nextInt(26));
			sb.append(c);
		}
		return sb.toString();
	}
}
